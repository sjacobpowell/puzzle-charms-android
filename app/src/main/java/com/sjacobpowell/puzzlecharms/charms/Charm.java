package com.sjacobpowell.puzzlecharms.charms;

import android.content.Context;
import android.widget.ImageView;

import com.sjacobpowell.puzzlecharms.R;

/**
 * Created by kamenomagic on 8/24/16.
 */
public class Charm extends ImageView {
    private CharmType type;

    public Charm(Context context, CharmType type) {
        super(context);
        this.type = type;
        setImageResource(getDrawableID());
    }

    public Charm(Context context, int typeDigit) {
        this(context, CharmType.getCharmType(typeDigit));
    }

    public CharmType getType() {
        return type;
    }

    public int getCharmTypeIndex() {
        return type.getIndex();
    }

    public int getDrawableID() {
        switch (type) {
            case SWIRL:
                return R.drawable.swirl;
            case BOW:
                return R.drawable.bow;
            case BOLT:
                return R.drawable.bolt;
            case FIRE:
                return R.drawable.fire;
            case HEART:
                return R.drawable.heart;
            case JIGSAW:
                return R.drawable.jigsaw;
            case CROWN:
                return R.drawable.crown;
            case STAR:
                return R.drawable.star;
            case WATER:
                return R.drawable.water;
            case SHOVEL:
                return R.drawable.shovel;
            default:
                return -1;
        }
    }
}
