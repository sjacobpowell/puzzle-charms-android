package com.sjacobpowell.puzzlecharms.charms;

/**
 * Created by kamenomagic on 8/24/16.
 */
public enum CharmType {
    NONE(-1), SWIRL(0), BOW(1), BOLT(2), FIRE(3), HEART(4), JIGSAW(5), CROWN(6), STAR(7), WATER(8), SHOVEL(9);
    private int index;

    CharmType(int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

    public static CharmType getCharmType(int index) {
        for(CharmType type : CharmType.values()) {
            if(type.getIndex() == index) {
                return type;
            }
        }
        return NONE;
    }
}
