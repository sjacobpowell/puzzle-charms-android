package com.sjacobpowell.puzzlecharms.grades;

import android.content.Context;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.sjacobpowell.puzzlecharms.R;

/**
 * Created by kamenomagic on 8/25/16.
 */
public class GradeMark extends ImageView {
    public GradeMark(Context context, boolean isBull) {
        super(context);
        setImageResource(isBull ? R.drawable.bull : R.drawable.cleot);
        setLayoutParams(new LinearLayout.LayoutParams(25, 25));
    }
}
