package com.sjacobpowell.puzzlecharms.grades;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by kamenomagic on 8/25/16.
 */
public class Grader {
    private Random random = new Random();
    private List<Integer> combination;

    public Grader() {
        combination = new ArrayList<Integer>();
        for(int i = 0; i < 4; i++) {
            int randomInt = random.nextInt(10);
            while(combination.contains(randomInt)) {
                randomInt = random.nextInt(10);
            }
            combination.add(randomInt);
        }
    }

    public Grade grade(List<Integer> guess) {
        Grade grade = new Grade();
        int guessDigit;
        int correctDigit;
        for(int i = 0; i < guess.size(); i++) {
            guessDigit = guess.get(i);
            correctDigit = combination.get(i);
            if(guessDigit == correctDigit) {
                grade.incrementBulls();
            } else if(combination.contains(guessDigit)) {
                grade.incrementCleots();
            }
        }
        return grade;
    }
}
