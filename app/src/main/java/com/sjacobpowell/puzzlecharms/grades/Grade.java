package com.sjacobpowell.puzzlecharms.grades;

/**
 * Created by kamenomagic on 8/25/16.
 */
public class Grade {
    private int bulls;
    private int cleots;

    public Grade() {
        this(0 , 0);
    }

    public Grade(int bulls, int cleots) {
        this.bulls = bulls;
        this.cleots = cleots;
    }

    public void incrementBulls() {
        bulls++;
    }

    public void incrementCleots() {
        cleots++;
    }

    public int getBulls() {
        return bulls;
    }

    public int getCleots() {
        return cleots;
    }
}
