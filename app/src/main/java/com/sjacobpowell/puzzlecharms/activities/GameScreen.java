package com.sjacobpowell.puzzlecharms.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.sjacobpowell.puzzlecharms.R;
import com.sjacobpowell.puzzlecharms.charms.Charm;
import com.sjacobpowell.puzzlecharms.grades.Grade;
import com.sjacobpowell.puzzlecharms.grades.GradeMark;
import com.sjacobpowell.puzzlecharms.grades.Grader;

import java.util.ArrayList;
import java.util.List;

public class GameScreen extends AppCompatActivity {
    private static final LinearLayout.LayoutParams preguessCharmLayoutParams = new LinearLayout.LayoutParams(200, 200);
    private static final LinearLayout.LayoutParams stagedGuessCharmLayoutParams = new LinearLayout.LayoutParams(125, 125);
    private static final LinearLayout.LayoutParams guessedCharmLayoutParams = new LinearLayout.LayoutParams(75, 75);
    private LinearLayout left;
    private LinearLayout right;
    private LinearLayout stagedGuess;
    private LinearLayout guesses;
    private Button guessButton;
    private ScrollView scrollView;
    private Grader grader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_screen);
        left = (LinearLayout) findViewById(R.id.charmsLayoutLeft);
        right = (LinearLayout) findViewById(R.id.charmsLayoutRight);
        stagedGuess = (LinearLayout) findViewById(R.id.stagedGuessLayout);
        guesses = (LinearLayout) findViewById(R.id.guessesLayout);
        guessButton = (Button) findViewById(R.id.guessButton);
        guessButton.setOnClickListener(new GuessButtonClickListener());
        scrollView = (ScrollView) findViewById(R.id.guessesScrollLayout);
        initializeCharms();
        resetGame();
    }

    private void resetGame() {
        unstageAllCharms();
        guesses.removeAllViews();
        grader = new Grader();
    }

    private void initializeCharms() {
        for (int i = 0; i < 5; i++) {
            Charm leftCharm = new Charm(this, i);
            Charm rightCharm = new Charm(this, i + 5);

            leftCharm.setLayoutParams(preguessCharmLayoutParams);
            rightCharm.setLayoutParams(preguessCharmLayoutParams);

            leftCharm.setOnClickListener(new CharmClickListener());
            rightCharm.setOnClickListener(new CharmClickListener());

            left.addView(leftCharm);
            right.addView(rightCharm);
        }
    }

    private void stageCharm(Charm charm) {
        (charm.getCharmTypeIndex() < 5 ? left : right).removeView(charm);
        charm.setLayoutParams(stagedGuessCharmLayoutParams);
        stagedGuess.addView(charm);
    }

    private void unstageCharm(Charm charm) {
        stagedGuess.removeView(charm);
        charm.setLayoutParams(preguessCharmLayoutParams);
        LinearLayout layout = charm.getCharmTypeIndex() < 5 ? left : right;
        int insertIndex = charm.getCharmTypeIndex();
        insertIndex = layout == right ? insertIndex - 5 : insertIndex;
        layout.addView(charm, insertIndex > layout.getChildCount() ? layout.getChildCount() : insertIndex);
    }

    private void unstageAllCharms() {
        while (stagedGuess.getChildCount() > 0) {
            unstageCharm((Charm) stagedGuess.getChildAt(0));
        }
    }

    private void guess() {
        LinearLayout guessLayout = new LinearLayout(GameScreen.this);
        List<Integer> guess = new ArrayList<Integer>();
        for (int i = 0; i < stagedGuess.getChildCount(); i++) {
            Charm charm = new Charm(GameScreen.this, ((Charm) stagedGuess.getChildAt(i)).getType());
            guess.add(charm.getCharmTypeIndex());
            charm.setLayoutParams(guessedCharmLayoutParams);
            guessLayout.addView(charm);
        }
        guessLayout.setGravity(Gravity.CENTER_VERTICAL);
        unstageAllCharms();
        guesses.addView(guessLayout);
        scrollView.postDelayed(new Runnable() {
            @Override
            public void run() {
                scrollView.fullScroll(ScrollView.FOCUS_DOWN);
            }
        }, 200);
        setGrade(guessLayout, grader.grade(guess));
    }

    private void setGrade(LinearLayout guessLayout, Grade grade) {
        for (int i = 0; i < grade.getBulls(); i++) {
            guessLayout.addView(new GradeMark(this, true));
        }
        for (int i = 0; i < grade.getCleots(); i++) {
            guessLayout.addView(new GradeMark(this, false));
        }
        if (grade.getBulls() == 4) {
            win();
        }
    }

    private void win() {
        new AlertDialog.Builder(this).setMessage(R.string.win_text)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        resetGame();
                    }
                }).create().show();
    }

    private class GuessButtonClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            view.setVisibility(Button.INVISIBLE);
            view.setEnabled(false);
            guess();
        }
    }

    private class CharmClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            if (view instanceof Charm) {
                Charm charm = (Charm) view;
                if (charm.getParent().equals(stagedGuess)) {
                    unstageCharm(charm);
                } else if (stagedGuess.getChildCount() < 4) {
                    stageCharm(charm);
                }
                boolean guessStaged = stagedGuess.getChildCount() == 4;
                guessButton.setVisibility(guessStaged ? Button.VISIBLE : Button.INVISIBLE);
                guessButton.setEnabled(guessStaged);
            }
        }
    }
}
